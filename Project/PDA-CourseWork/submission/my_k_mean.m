function [cluster, centroids] = my_k_mean( k, dataset, distance_type )
    [r, c] = size(dataset);
    centroids = zeros(k, c); % k Centroids
    for i = 1:k
        centroids(i, :) = dataset(round(rand * r), :); % Initialize to random points in dataset
    end
    
    new_centroids = zeros(k, c);
    distance_matrix = zeros(r, k);
    iteration = 1;
    x_points = zeros;
    y_points = zeros;
    
    while(not(all(all(centroids == new_centroids)))) % Repeat until the centroids do not change
        new_centroids = centroids;
        for i = 1:k
            rep_centroid = repmat(centroids(i,:), r, 1);
            if(all(size(distance_type) == size('pearson')) && all(distance_type == 'pearson'))
                distance = pearson_distance(dataset, rep_centroid);
            else
                distance = euclidean_distance(dataset, rep_centroid);
            end
            distance_matrix(:, i) = distance; % Every column in distance_matrix gives distance of each point from ith centroid
        end

        [~, cluster] = min(distance_matrix, [], 2);
        for i = 1:k
            s = sum(cluster == i);
            if(s > 0)
                centroids(i, :) = sum(dataset(cluster == i, :)) / sum(cluster == i);
            else
                centroids(i, :) = new_centroids(i, :);
            end
        end
        x_points(iteration) = iteration; % Accumulate iteration number
        y_points(iteration) = sum(min(distance_matrix, [], 2)); % Accumulate distances
        iteration = iteration + 1;
    end
    
    clf;
    line(x_points, y_points);
    title('SSD vs Iterations');
    xlabel('Iterations');
    ylabel('Sum of Squared Distances');
end
