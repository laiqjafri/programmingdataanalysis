function [ distance ] = pearson_distance( x, y )
    [~, x_cols] = size(x);
    [~, y_cols] = size(y);
    x_mean = sum(x, 2) / x_cols;
    y_mean = sum(y, 2) / y_cols;
    
    x_mean = repmat(x_mean, 1, x_cols);
    y_mean = repmat(y_mean, 1, y_cols);
    
    x_diff = x - x_mean;
    y_diff = y - y_mean;
    numerator = sum(x_diff .* y_diff, 2);
    
    x_diff_sq = power(x - x_mean, 2);
    y_diff_sq = power(y - y_mean, 2);
    denominator = sqrt(sum(x_diff_sq, 2)) .* sqrt(sum(y_diff_sq, 2));
    
    distance = 1 - numerator ./ denominator;
end

