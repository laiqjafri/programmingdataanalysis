function draw_diseases_graph( dataset, labels, cluster, centroids)
    [k, ~]  = size(centroids);
    figure;
    colors = generate_colors;
    for iClass = 1:k
        filename = strcat('clusters/disease_cluster_', int2str(iClass), '.txt');
        classIdx = cluster == iClass;
        fid = fopen(filename, 'wt');
        fprintf(fid, '%s\n', labels{classIdx, end});
        fclose(fid);
        scatter3(dataset(classIdx,1),dataset(classIdx,2),dataset(classIdx,3), 30, colors(iClass,:), 'filled');
        hold on;
    end
    title('3D Visualization wrt Clusters');
    xlabel('Column 1');
    ylabel('Column 2');
    zlabel('Column 3');
    legend(strcat('CLUSTER', int2str((1:k)')));
    hold off;
    
    visualization_wrt_diseases(dataset, labels);
end

