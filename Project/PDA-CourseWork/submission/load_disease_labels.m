function [labels] = load_disease_labels()
    % Load disease labels
    labels = importdata('labels');
end
