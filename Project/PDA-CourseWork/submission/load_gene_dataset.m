function [dataset] = load_gene_dataset()
    % Load genes data and preprocess it
    dataset = importdata('EcoliDatasetCW5810_truncated.txt');
    dataset = dataset.data;
    dataset = (dataset(:,1:3:end) + dataset(:,2:3:end) + dataset(:,3:3:end)) / 3;
end
