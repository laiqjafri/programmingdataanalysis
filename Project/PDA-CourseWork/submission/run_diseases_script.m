function run_diseases_script(k, distance_type)
    if nargin < 1
        k = 5;
    end
    
    if nargin < 2
        distance_type = 'euclidean';
    end
    
    common_setup;
    labels = load_disease_labels;
    dataset = load_disease_dataset;
    [cluster, centroids] = my_k_mean(k, dataset, distance_type);
    draw_disease_graphs(dataset, labels, cluster, centroids);
end
