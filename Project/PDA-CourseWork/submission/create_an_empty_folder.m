function create_an_empty_folder(folder_name)

% Creates and empty folder in the current directory
% with name = folder_name

    if(exist(folder_name, 'dir'))
        rmdir(folder_name, 's');
    end
    mkdir(folder_name);
end

