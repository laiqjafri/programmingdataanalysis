function run_genes_script(k, distance_type)
    if nargin < 1
        k = 5;
    end
    
    if nargin < 2
        distance_type = 'euclidean';
    end
    
    common_setup;
    labels = load_gene_labels;
    dataset = load_gene_dataset;
    [cluster, centroids] = my_k_mean(k, dataset, distance_type);
    draw_genes_graph(dataset, labels, cluster, centroids);
end