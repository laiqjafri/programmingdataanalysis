function [ distance ] = euclidean_distance( x, y )
    % Returns Euclidean distance vector between two matrices
    distance = sqrt(sum(power(x - y, 2), 2));
end

