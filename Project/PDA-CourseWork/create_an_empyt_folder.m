function create_an_empyt_folder(folder_name)
%CREATE_AN_EMPYT_FOLDER Creates and empty folder in the current directory
%with name = folder_name
    if(exist(folder_name, 'dir'))
        rmdir(folder_name, 's');
    end
    mkdir(folder_name);
end

