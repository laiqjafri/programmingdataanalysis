function [labels] = load_gene_labels()
    % Load gene profile identifiers
    dataset = importdata('EcoliDatasetCW5810_truncated.txt');
    labels = dataset.textdata(:, 1);
end
