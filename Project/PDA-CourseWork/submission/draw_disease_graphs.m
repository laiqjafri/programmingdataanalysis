function draw_disease_graphs( dataset, labels, cluster, centroids)
    visualization_wrt_clusters(dataset, labels, centroids, cluster);
    visualization_wrt_diseases(dataset, labels);
end

