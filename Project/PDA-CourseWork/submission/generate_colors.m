function colors = generate_colors
    % Different colors to be used in generating graphs
    colors = [
        0.9020         0    0.6000
        0.7529    0.2471    0.2471
        0.1608    0.6392    0.1608
        0.8980    0.9020         0
        0.2196    0.8824    0.6902
        0    0.3216    0.8000
        0.5 0.5 0.5
        0.8000    1.0000    0.8000
        0.6 0.8 1
        0.8000    0.6000    1.0000
        ];
end
