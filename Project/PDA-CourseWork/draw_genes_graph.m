function draw_genes_graph(dataset, labels, cluster, centroids)
    [k, ~]  = size(centroids);
    figure;
    suptitle(strcat('EACH COLUMN AGAINST THE OTHER FOR K = ', int2str(k), '.txt'));
    plot_number = 1;
    colors = generate_colors;
    for iClass = 1:k
        classIdx = cluster == iClass;
        filename = strcat('clusters/genes_cluster_', int2str(iClass));
        fid = fopen(filename, 'wt');
        fprintf(fid, '%s\n', labels{classIdx, end});
        fclose(fid);     
    end
    
    for i = 1:5
       for j = (i+1):6;
           for iClass = 1:k
               classIdx = cluster == iClass;
               subplot(3, 5, plot_number);
               scatter(dataset(classIdx, i), dataset(classIdx, j),30, colors(iClass,:), 'filled');
               xlabel(strcat('Column ', int2str(i)));
               ylabel(strcat('Column ', int2str(j)));
               hold on;
           end
           plot_number = plot_number + 1;
       end
    end
    hold off;
    
    figure;
    hold on;
    dataset = scaledata(dataset, -2, 2);
    ylim([-3 3]);
    for iClass = 1:k
        classIdx = cluster == iClass;
        if any(classIdx)
            plot(1:6, dataset(classIdx, :), 'Color', colors(iClass, :));
        end
        hold on;
    end
    title('GENE PROFILE EXPRESSIONS AGAINST TIME');
    xlabel('Time');
    ylabel('Gene Expression Value');
    hold off;
end