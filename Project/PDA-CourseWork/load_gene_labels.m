function [labels] = load_gene_labels()
    dataset = importdata('EcoliDatasetCW5810_truncated.txt');
    labels = dataset.textdata(:, 1);
end