function visualization_wrt_diseases(dataset, labels)
    figure;
    diseases = unique(labels);
    [total_diseases, ~] = size(diseases);
    colors = generate_colors;
    for i = 1:total_diseases
        classIdx = strcmp(diseases(i, 1), labels);
        scatter3(dataset(classIdx,1),dataset(classIdx,2),dataset(classIdx,3), 30, colors(i, :), 'filled');
        hold on;
    end
    title('3D Visualization wrt Diseases');
    xlabel('Column 1');
    ylabel('Column 2');
    zlabel('Column 3');
    legend(diseases);
    hold off;
end

