function [ distance ] = euclidean_distance( x, y )
    distance = sqrt(sum(power(x - y, 2), 2));
end

