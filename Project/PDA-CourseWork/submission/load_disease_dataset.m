function [dataset] = load_gene_dataset()
    % Load diseases data
    dataset = importdata('3d_filtered_outfile');
end
